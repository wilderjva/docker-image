package com.example.demoimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class DemoImageApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoImageApplication.class, args);
	}


}

@RequestMapping("hello")
@RestController
class HelloController {
	@GetMapping
	public String getHelloMessage() {
		return "Hola";
	}
}
